ARG VERSION=8.0-alpine-amd64

FROM mcr.microsoft.com/dotnet/sdk:$VERSION
RUN apk add --no-cache --update nodejs npm
RUN node --version
RUN npm --version

