# dotnet-build

Project housing the base Docker image used to build my dotnet projects.
 - dotnet core sdk
 - nodejs
 - npm

Project house the base Docker image used to run my dotnet projects.
 - dotnet core runtime
 - base env vars
 - running as appuser
