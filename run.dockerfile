FROM mcr.microsoft.com/dotnet/aspnet:8.0-alpine-amd64
WORKDIR /app

ENV DOTNET_CLI_TELEMETRY_OPTOUT=1
ENV ASPNETCORE_ENVIRONMENT=Production
ENV ASPNETCORE_URLS=http://*:5000
ENV ASPNETCORE_HTTP_PORTS=5000
ENV DOTNET_RUNNING_IN_CONTAINER=true
ENV DOTNET_EnableDiagnostics=0

RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser
